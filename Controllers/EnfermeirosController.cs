using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;
using Microsoft.AspNetCore.Authorization;

namespace Hospital.Controllers
{
    [Route("api/[controller]")]
    [Authorize()]
    [ApiController]
    public class EnfermeirosController : ControllerBase
    {
        private readonly ProjectContext _context;

        public EnfermeirosController(ProjectContext context)
        {
            _context = context;
        }

        // GET: api/Enfermeiros
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Enfermeiros>>> GetEnfermeiros()
        {
            return await _context.Enfermeiros.ToListAsync();
        }

        // GET: api/Enfermeiros/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Enfermeiros>> GetEnfermeiros(string id)
        {
            var enfermeiros = await _context.Enfermeiros.FindAsync(id);

            if (enfermeiros == null)
            {
                return NotFound();
            }

            return enfermeiros;
        }

        // GETEnfermeirosTriagem
        [HttpGet("triagens/{id}")]
        public async Task<ActionResult<IEnumerable<Triagem>>> GetEnfermeirosTriagem(string id)
        {
            var triagens = await _context.Triagem.Where(p => p.Coren.Equals(id)).ToListAsync();

            if(triagens==null){
                return NotFound();
            }

            else return triagens;
        }


        //GETConsultasComTriagem
        [HttpGet("consultascomenfermeiros/{id}")]
        public async Task<ActionResult<IEnumerable<Consultas>>> GetConsultasComEnfermeiros(string id)
        {
            var consultas = await _context.Consultas.Where(p => p.Coren.Equals(id)).ToListAsync();

            if (consultas == null)
            {
                return NotFound();
            }

            else return consultas;
        }
        // PUT: api/Enfermeiros/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEnfermeiros(string id, Enfermeiros enfermeiros)
        {
            if (id != enfermeiros.Coren)
            {
                return BadRequest();
            }

            _context.Entry(enfermeiros).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EnfermeirosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Enfermeiros
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Enfermeiros>> PostEnfermeiros(Enfermeiros enfermeiros)
        {
            _context.Enfermeiros.Add(enfermeiros);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EnfermeirosExists(enfermeiros.Coren))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction(nameof(GetEnfermeiros), new { id = enfermeiros.Coren }, enfermeiros);
        }

        // DELETE: api/Enfermeiros/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Enfermeiros>> DeleteEnfermeiros(string id)
        {
            var enfermeiros = await _context.Enfermeiros.FindAsync(id);
            if (enfermeiros == null)
            {
                return NotFound();
            }

            _context.Enfermeiros.Remove(enfermeiros);
            await _context.SaveChangesAsync();

            return enfermeiros;
        }

        private bool EnfermeirosExists(string id)
        {
            return _context.Enfermeiros.Any(e => e.Coren == id);
        }
    }
}
