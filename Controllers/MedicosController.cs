using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;
using Microsoft.AspNetCore.Authorization;

namespace Hospital.Controllers
{
    [Route("api/[controller]")]
    [Authorize()]
    [ApiController]
    public class MedicosController : ControllerBase
    {
        private readonly ProjectContext _context;

        public MedicosController(ProjectContext context)
        {
            _context = context;
        }

        // GET: api/Medicos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Medicos>>> GetMedicos()
        {
            return await _context.Medicos.ToListAsync();
        }

        // GETbyEspecialidade
        [HttpGet("espcialidade/{id:int}")]
        public async Task<ActionResult<IEnumerable<Medicos>>> GetMedicosEspecialidades(int id)
        {
            var medicos = await _context.Medicos.Where(e => e.CodEspecialidade == id).ToListAsync();

             if(medicos==null){
                return NotFound();
            }

            else return medicos;
        }
        
        [HttpGet("consulta/{id}")]
        public async Task<ActionResult<IEnumerable<Consultas>>> GetMedicoConsultas(string id)
        {
            var consultas = await _context.Consultas.Where(e => e.Crm.Equals(id)).ToListAsync();

            if(consultas==null){
                return NotFound();
            }

            else return consultas;
        }

        // GET: api/Medicos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Medicos>> GetMedicos(string id)
        {
            var medicos = await _context.Medicos.FindAsync(id);

            if (medicos == null)
            {
                return NotFound();
            }

            return medicos;
        }


        // PUT: api/Medicos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicos(string id, Medicos medicos)
        {
            if (id != medicos.Crm)
            {
                return BadRequest();
            }

            _context.Entry(medicos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medicos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Medicos>> PostMedicos(Medicos medicos)
        {
            _context.Medicos.Add(medicos);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MedicosExists(medicos.Crm))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction(nameof(GetMedicos), new { id = medicos.Crm }, medicos);
        }

        // DELETE: api/Medicos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Medicos>> DeleteMedicos(string id)
        {
            var medicos = await _context.Medicos.FindAsync(id);
            if (medicos == null)
            {
                return NotFound();
            }

            _context.Medicos.Remove(medicos);
            await _context.SaveChangesAsync();

            return medicos;
        }

        private bool MedicosExists(string id)
        {
            return _context.Medicos.Any(e => e.Crm == id);
        }
    }
}
