using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;
using Microsoft.AspNetCore.Authorization;

namespace Hospital.Controllers
{
    [Route("api/[controller]")]
    [Authorize()]
    [ApiController]
    public class TriagemController : ControllerBase
    {
        private readonly ProjectContext _context;

        public TriagemController(ProjectContext context)
        {
            _context = context;
        }

        // GET: api/Triagem
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Triagem>>> GetTriagem()
        {
            return await _context.Triagem.ToListAsync();
        }

        [HttpGet("prioridade/{id:int}")]
        public async Task<ActionResult<IEnumerable<Triagem>>> GetTriagemEspecialidade(int id)
        {  
            var triagens = await _context.Triagem.Where(t => t.Prioridade == id).ToListAsync();

            if(triagens==null){
                return NotFound();
            }

            return triagens;
        }

        // GET: api/Triagem/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Triagem>> GetTriagem(int id)
        {
            var triagem = await _context.Triagem.FindAsync(id);

            if (triagem == null)
            {
                return NotFound();
            }

            return triagem;
        }
        
        // PUT: api/Triagem/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTriagem(int id, Triagem triagem)
        {
            if (id != triagem.CodTriagem)
            {
                return BadRequest();
            }

            _context.Entry(triagem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TriagemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Triagem
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Triagem>> PostTriagem(Triagem triagem)
        {
            _context.Triagem.Add(triagem);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetTriagem), new { id = triagem.CodTriagem }, triagem);
        }

        // DELETE: api/Triagem/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Triagem>> DeleteTriagem(int id)
        {
            var triagem = await _context.Triagem.FindAsync(id);
            if (triagem == null)
            {
                return NotFound();
            }

            _context.Triagem.Remove(triagem);
            await _context.SaveChangesAsync();

            return triagem;
        }

        private bool TriagemExists(int id)
        {
            return _context.Triagem.Any(e => e.CodTriagem == id);
        }
    }
}
