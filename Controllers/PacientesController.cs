using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Hospital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PacientesController : ControllerBase
    {
        private readonly ProjectContext _context;

        public PacientesController(ProjectContext context)
        {
            _context = context;
        }

        // GET: api/Pacientes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Pacientes>>> GetPacientes()
        {
            return await _context.Pacientes.ToListAsync();
        }


        // GETbySexo
        [HttpGet("sexo/{id}")]
        public async Task<ActionResult<IEnumerable<Pacientes>>> GetPacientesBytlSexo(string id)
        {
            var pacientes = await _context.Pacientes.Where(p => p.Sexo.Equals(id)).ToListAsync();

            if(pacientes==null){
                return NotFound();
            }

            else return pacientes;
        }

        // GETPacienteConsultas
        [HttpGet("consulta/{id}")]
        public async Task<ActionResult<IEnumerable<Consultas>>> GetPacienteConsultas(string id)
        {
            var consultas = await _context.Consultas.Where(c => c.Cpf.Equals(id)).ToListAsync();

            if(consultas==null){
                return NotFound();
            }

            return consultas;
        }

        // GETPacienteTriagem
        [HttpGet("triagem/{id}")]
        public async Task<ActionResult<IEnumerable<Triagem>>> GetPacienteTrigens(string id)
        {
            var triagens = await _context.Triagem.Where(t => t.Cpf.Equals(id)).ToListAsync();

            if(triagens==null){
                return NotFound();
            }

            return triagens;
        }

        // GET: api/Pacientes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Pacientes>> GetPacientes(string id)
        {
            var pacientes = await _context.Pacientes.FindAsync(id);

            if (pacientes == null)
            {
                return NotFound();
            }

            return pacientes;
        }

        // PUT: api/Pacientes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPacientes(string id, Pacientes pacientes)
        {
            if (id != pacientes.Cpf)
            {
                return BadRequest();
            }

            _context.Entry(pacientes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PacientesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Pacientes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Pacientes>> PostPacientes(Pacientes pacientes)
        {
            _context.Pacientes.Add(pacientes);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PacientesExists(pacientes.Cpf))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction(nameof(GetPacientes), new { id = pacientes.Cpf }, pacientes);
        }

        // DELETE: api/Pacientes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Pacientes>> DeletePacientes(string id)
        {
            var pacientes = await _context.Pacientes.FindAsync(id);
            if (pacientes == null)
            {
                return NotFound();
            }

            _context.Pacientes.Remove(pacientes);
            await _context.SaveChangesAsync();

            return pacientes;
        }

        private bool PacientesExists(string id)
        {
            return _context.Pacientes.Any(e => e.Cpf == id);
        }
    }
}
