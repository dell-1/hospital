using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;
using Microsoft.AspNetCore.Authorization;

namespace Hospital.Controllers
{
    [Route("api/[controller]")]
    [Authorize()]
    [ApiController]
    public class ConsultasController : ControllerBase
    {
        private readonly ProjectContext _context;

        public ConsultasController(ProjectContext context)
        {
            _context = context;
        }

        // GET: api/Consultas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Consultas>>> GetConsultas()
        {
            return await _context.Consultas.ToListAsync();
        }

        // GET: api/Consultas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Consultas>> GetConsultas(int id)
        {
            var consultas = await _context.Consultas.FindAsync(id);

            if (consultas == null)
            {
                return NotFound();
            }

            return consultas;
        }

        // PUT: api/Consultas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConsultas(int id, Consultas consultas)
        {
            if (id != consultas.CodConsultas)
            {
                return BadRequest();
            }

            _context.Entry(consultas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConsultasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Consultas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Consultas>> PostConsultas(Consultas consultas)
        {
            _context.Consultas.Add(consultas);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetConsultas), new { id = consultas.CodConsultas }, consultas);
        }

        // DELETE: api/Consultas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Consultas>> DeleteConsultas(int id)
        {
            var consultas = await _context.Consultas.FindAsync(id);
            if (consultas == null)
            {
                return NotFound();
            }

            _context.Consultas.Remove(consultas);
            await _context.SaveChangesAsync();

            return consultas;
        }

        private bool ConsultasExists(int id)
        {
            return _context.Consultas.Any(e => e.CodConsultas == id);
        }
    }
}
