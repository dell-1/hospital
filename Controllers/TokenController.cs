using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Hospital.Models;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;


namespace Hospital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ProjectContext _context;
        public TokenController(IConfiguration configuration, ProjectContext context)
        {   
            _context = context;
            _configuration = configuration;
        }

        [AllowAnonymous]
        [HttpPost("medicoLogin")]
        public async Task<IActionResult> RequestMedicosToken([FromBody] Medicos request)
        {
            var medicos = await _context.Medicos.ToListAsync();

            List<String> listNomes = new List<string>();
            List<String> listIds = new List<string>();

            foreach (var medico in medicos)
            {
                listNomes.Add(medico.Nome);
                listIds.Add(medico.Crm);
            }

            if(listNomes.Contains(request.Nome) && listIds.Contains(request.Crm))
            {
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, request.Nome),
                    new Claim(ClaimTypes.Role, "Medico")
                };
                
                var key = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(_configuration["SecurityKey"])
                );
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: "authenticationController",
                    audience: "empregados",
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(60),
                    signingCredentials: creds
                );

                return Ok(
                    new{ token = new JwtSecurityTokenHandler().WriteToken(token) }
                );
            }

            return BadRequest("Morreu...");
        }

        [AllowAnonymous]
        [HttpPost("enfermeiroLogin")]
        public async Task<IActionResult> RequestEnfermeirosToken([FromBody] Enfermeiros request)
        {
            var enfermeiros = await _context.Enfermeiros.ToListAsync();

            List<String> listNomes = new List<string>();
            List<String> listIds = new List<string>();

            foreach (var enfermeiro in enfermeiros)
            {
                listNomes.Add(enfermeiro.Nome);
                listIds.Add(enfermeiro.Coren);
            }

            if(listNomes.Contains(request.Nome) && listIds.Contains(request.Coren))
            {
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, request.Nome),
                    new Claim(ClaimTypes.Role, "Enfermeiro")
                };
                
                var key = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(_configuration["SecurityKey"])
                );
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: "authenticationController",
                    audience: "empregados",
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(60),
                    signingCredentials: creds
                );

                return Ok(
                    new{ token = new JwtSecurityTokenHandler().WriteToken(token) }
                );
            }

            return BadRequest("Morreu...");
        }
    }
} 